<?php
/*
 * Plugin Name: Artsy Geek Theme Support
 * Version: 1.0
 * Plugin URI: http://www.artsygeek.com/
 * Description: Theme support plugin for Artsy Geek crafted WordPress themes.
 * Author: Artsy Geek
 * Author URI: http://www.artsygeek.com/
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: artsygeek-theme-support
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Artsy Geek
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Load theme support class files
require_once( 'includes/class-artsygeek-theme-support.php' );

// Load theme support libraries
require_once( 'includes/lib/class-artsygeek-theme-support-post-type.php' );
require_once( 'includes/lib/class-artsygeek-theme-support-taxonomy.php' );

// Load Third party libraries

// Load theme widgets
require_once( 'includes/widgets/class-artsygeek-contact-widget.php' );
require_once( 'includes/widgets/class-artsygeek-image-button-widget.php' );
require_once( 'includes/widgets/class-artsygeek-social-media-widget.php' );

// Load theme customizer controls
require_once 'includes/wordpress-theme-customizer-custom-controls/theme-customizer.php';

// Load ACF theme options
require_once( 'includes/artsygeek-acf-theme-options.php' );

/**
 * Returns the main instance of artsygeek-theme-support to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object artsygeek-theme-support
 */
function Artsygeek_Theme_Support() {
	
  $instance = Artsygeek_Theme_Support::instance( __FILE__, '1.0.0' );

	return $instance;
}

Artsygeek_Theme_Support();
