<?php


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
add_action( 'widgets_init', function(){
     register_widget( 'artsygeek_social_media_widget' );
});	

/**
 * Adds artsygeek_social_media_widget widget.
 */
class artsygeek_social_media_widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'artsygeek_social_media_widget', // Base ID
			__('Social Media', 'artsygeek-theme-support'), // Name
			array( 'description' => __( 'Display your social media links.', 'artsygeek-theme-support' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		
		extract( $args );

        echo $before_widget;
		
		$title = apply_filters( 'widget_title', $instance['title'] );

		$social_sites = theme_customizer::social_array();

	    foreach ( $social_sites as $social_site => $profile ) {

	        if ( strlen( get_theme_mod( $social_site ) ) > 0 ) {
	            $active_sites[ $social_site ] = $social_site;
	        }
	    }

	    if ( ! empty( $active_sites ) ) {

			if ( $title ) {
				echo $before_title . $title . $after_title;
			}
	        echo "<ul class='social-media-links' itemscope itemtype='https://schema.org/Organization'>";
	        foreach ( $active_sites as $key => $active_site ) {

	        	if ( in_array($active_site, ['facebook','twitter','pinterest','google-plus','instagram','linkedin'] ) ) {
	        	}

	            $class = 'fa fa-' . $active_site;
	            ?>
	            <li>
	                <a class="fa-lg" itemprop="sameAs" class="<?php echo esc_attr( $active_site ); ?>" target="_blank" href="<?php echo esc_url( get_theme_mod( $key ) ); ?>">
	                    <i class="<?php echo esc_attr( $class ); ?>" aria-hidden="true" title="<?php echo esc_attr( $active_site ); ?>"></i>
	                </a>
	            </li>
	            <?php
	        }
	        echo "</ul>";
	    }

        echo $after_widget;
	
	}
	
	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}

		else {
			$title = __( 'Join Us', 'artsygeek-theme-support' );
		}

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        return $instance;
	}

} // class artsygeek_social_media_widget