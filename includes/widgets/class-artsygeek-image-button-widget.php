<?php


// Block direct requests
if ( !defined('ABSPATH') )
    die('-1');


add_action( 'widgets_init', function(){
    register_widget("artsygeek_image_button_widget"); 
});

class artsygeek_image_button_widget extends WP_Widget {
    
    /**
     * The version number.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $_version;

    /**
     * The token.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $_token;


    /**
     * Constructor
     **/
    public function __construct( $file = '', $version = '1.0.0' )
    {   
        // Load plugin environment variables
        $this->_version = $version;
        $this->_token = 'artsygeek-theme-support';

        $widget_ops = array(
            'classname' => $this->_token,
            'description' => 'Add a linked image with a button.'
        );

        parent::__construct( 
            'artsygeek_image_button_widget', 
            __('Image with a Button', 'artsygeek-theme-support'), 
            $widget_ops 
        );
    }

    /**
     * Outputs the HTML for this widget.
     *
     * @param array  An array of standard parameters for widgets in this theme
     * @param array  An array of settings for this widget instance
     * @return void Echoes it's output
     **/
    public function widget( $args, $instance )
    {   
        $title = empty( $instance['title'] ) ? get_bloginfo('name') :  $instance['title'];

        $sanitized_title = sanitize_title($title);

        $button_text = empty( $instance['button_text'] ) ? 'GO!' :  $instance['button_text'];

        $sanitized_button_text = esc_html($button_text);

        $link = empty( $instance['link'] ) ? get_home_url() : $instance['link'];

        echo $args['before_widget'] . '<div class="artsygeek-image-button-widget-container">';

        if ( ! empty( $instance['image'] && !empty( $link ) ) ) {
            
            echo '<a class="artsygeek-image-button-link" href="' . $link . '" target="_blank">';
            echo '<img class="' . $sanitized_title . '-image img-responsive" src="' . $instance['image'] . '" alt="'. $title .'image" >'; 
            echo '</a>';

            if(!empty($button_text)) :

                echo '<a class="artsygeek-image-button-btn btn" href="' . $link . '" target="_blank">';
                echo $sanitized_button_text; 
                echo '</a>';

            endif;

        } else {
            echo '<span class="alert">Please set an image and link in this widget\'s admin.</span>';
        }


        echo '</div>' . $args['after_widget'];
    }

    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param array  An array of new settings as submitted by the admin
     * @param array  An array of the previous settings
     * @return array The validated and (if necessary) amended settings
     **/
    public function update( $new_instance, $old_instance ) {

        $instance = $old_instance;

        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        
        $instance['button_text'] = sanitize_text_field( $new_instance['button_text'] );

        $instance['link'] = esc_url_raw( $new_instance['link'] );
        
        $instance['image'] = $new_instance['image'];

        return $instance;
    }

    /**
     * Displays the form for this widget on the Widgets page of the WP Admin area.
     *
     * @param array  An array of the current settings for this widget
     * @return void
     **/
    public function form( $instance )
    {
         $defaults = array( 
            'title' => __(''), 
            'link' => __(''), 
            'image' =>  __(''),
            'button_text' => __(''),
        );

        $instance = wp_parse_args( (array) $instance, $defaults);   

        $title = $instance['title'];

        $button_text = $instance['button_text'];
        
        $link = $instance['link'];

        $image = $instance['image'];
        ?>
        <p>
            <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:', $this->_token ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_name( 'button_text' ); ?>"><?php _e( 'Button Text:', $this->_token ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'button_text' ); ?>" name="<?php echo $this->get_field_name( 'button_text' ); ?>" type="text" value="<?php echo esc_attr( $button_text ); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_name( 'link' ); ?>"><?php _e( 'Link:', $this->_token ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_name( 'image' ); ?>"><?php _e( 'image:', $this->_token ); ?></label>
            <input name="<?php echo $this->get_field_name( 'image' ); ?>" id="<?php echo $this->get_field_id( 'image' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $image ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
    <?php
    }
}
?>