<?php


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
add_action( 'widgets_init', function(){
     register_widget( 'artsygeek_contact_widget' );
});	

/**
 * Adds artsygeek_contact_widget widget.
 */
class artsygeek_contact_widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'artsygeek_contact_widget', // Base ID
			__('Business Address', 'artsygeek-theme-support'), // Name
			array( 'description' => __( 'Your business address from the contact page.', 'artsygeek-theme-support' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		
		if (isset($instance[ 'contact_page' ])) { 
			
			$post_id = $instance[ 'contact_page' ];

			// Assign location/address info from ACF fields.
			$location = get_field('location', $post_id);
		  	$address_street = get_field('address_street', $post_id);
		  	$address_city = get_field('address_city', $post_id);	  
		  	$address_state = get_field('address_state', $post_id);
		  	$address_zip = get_field('address_zip', $post_id);
			
			$email = get_field('email_address', $post_id);
			$phone_clean = preg_replace('/\D+/', '', $phone);

			$address_street = empty( $address_street) ? '24 Second Avenue' : $address_street;
			$address_city = empty( $address_city ) ? 'San Mateo' : $address_city;

			$address_state = empty( $address_state) ? 'CA' : $address_state;
			$address_zip = empty( $address_zip ) ? '94401' : $address_zip;


			$address = '<address>
							<p translate="no" typeof="schema:PostalAddress">
								<span class="address-street" property="schema:streetAddress">%s</span>
								<span class="address-locality" property="schema:addressLocality">%s,</span> 
								<abbr title="California" property="schema:addressRegion">%s</abbr> 
								<span class="address-zip" property="schema:postalCode">%s</span> 
							</p>
						</address>';

			$address = sprintf($address, $address_street, $address_city, $address_state, $address_zip );


		if( !empty($location) ):
		?>
		<section class="contact-address-container">
		<?php 	

			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
			} 
			
			echo $address;
			
			if ( have_rows('phone_numbers', $post_id) ) : 
				while( have_rows('phone_numbers', $post_id) ): the_row(); 
				$label = get_sub_field('label', $post_id);
				$phone = get_sub_field('number', $post_id);
				$phone_clean = preg_replace('/\D+/', '', $phone);
				$label = empty( $label ) ? 'phone:' : $label;
				echo '<h5>' . $label . '</h5>';
				echo '<p><a class="phone" href="tel:' . $phone_clean . '" itemprop="telephone">' . $phone . '</a></p>';
				endwhile; 
			endif; 

			if ( !empty($email) ) : 
				echo '<h5>Email</h5>';
				echo '<p><a class="email" href="mailto:' . antispambot($email) . '">' . antispambot($email) . '</a></p>';
			endif;

		?>
		</section>

		<?php endif; 
		}
	
	}
	
	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}

		else {
			$title = __( 'Business Address', 'artsygeek-theme-support' );
		}

		if ( isset( $instance[ 'contact_page' ] ) ) {
			$contact_page = $instance[ 'contact_page' ];
		}

		else {
			$contact_page = '';
		}

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
	        <label for="<?php echo $this->get_field_id('contact_page'); ?>"><?php _e('Set Contact Page:'); ?></label>
	        <?php wp_dropdown_pages(array(
			    'id' => $this->get_field_id('contact_page'),
			    'name' => $this->get_field_name('contact_page'),
			    'selected' => $instance['contact_page'],
			) ); ?>
	    </p>
		
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
	 	$instance['contact_page'] = strip_tags($new_instance['contact_page']);
        return $instance;
	}

} // class artsygeek_contact_widget