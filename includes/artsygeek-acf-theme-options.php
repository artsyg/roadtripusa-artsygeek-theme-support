<?php

/**
* Add Site Options with ACF
* Settings fields are managed in the Custom Fields admin interface
*/
add_action('acf/init', 'artsygeek_acf_add_options_page');

function artsygeek_acf_add_options_page() {

    if( function_exists('acf_add_options_page') ) {
    
        $parent = acf_add_options_page(array(
            'page_title'    => 'Routes',
            'menu_title'    => 'Routes',
            'menu_slug'     => 'rtusa-route-settings',
            'capability'    => 'edit_posts',
            'icon_url'      => 'dashicons-location-alt',
            'position'      => '32.1225',
            'redirect'      => false
        ));

        acf_add_options_sub_page(array(
            'page_title'    => 'Menu Settings',
            'menu_title'    => 'Menu Settings',
            'parent_slug'   => $parent['menu_slug'],
        ));
    }
}


/**
 * Modify Routes Settings admin page relationship search to only search for post titles only.
 * @link    http://wordpress.stackexchange.com/a/11826/1685
 * @param   string      $search
 * @param   WP_Query    $wp_query
 */
add_filter( 'posts_search', 'artsygeek_search_by_title', 500, 2 );

function artsygeek_search_by_title( $search, $wp_query ) {
    
    if ( !is_admin() )
        return;
    
    if ( ! empty( $search ) && ! empty( $wp_query->query_vars['search_terms'] ) ) {

        global $wpdb;

        $q = $wp_query->query_vars;
        $n = ! empty( $q['exact'] ) ? '' : '%';

        $search = array();

        foreach ( ( array ) $q['search_terms'] as $term )
            $search[] = $wpdb->prepare( "$wpdb->posts.post_title LIKE %s", $n . $wpdb->esc_like( $term ) . $n );

        if ( ! is_user_logged_in() )
            $search[] = "$wpdb->posts.post_password = ''";

        $search = ' AND ' . implode( ' AND ', $search );
    }

    return $search;
}


